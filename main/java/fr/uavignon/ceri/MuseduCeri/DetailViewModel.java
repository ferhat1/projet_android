package fr.uavignon.ceri.MuseduCeri;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.MuseduCeri.data.Item;
import fr.uavignon.ceri.MuseduCeri.data.ItemRepository;
import fr.uavignon.ceri.MuseduCeri.data.webservice.ItemResult;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private ItemRepository repository;
    private MutableLiveData<Item> item;
    MutableLiveData<ItemResult> result;
    //Il stocke les informations outils pour la vue
    //LiveData pour l'affichage dynamique.
    public DetailViewModel (Application application) {
        super(application);
        repository = ItemRepository.get(application);
        item = new MutableLiveData<>();
        result = repository.getResult();
    }



    public void setItem(String key) {
        repository.getItem(key);
        item = repository.getSelectedItem();
    }

    LiveData<Item> getItem() {
        return item;
    }
}

