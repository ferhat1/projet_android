package fr.uavignon.ceri.MuseduCeri.data.webservice;

import java.util.List;

public class ItemResponse {
    public final String description=null;
    public final List<Integer> timeFrame=null;
    public final String name=null;
    public final String brand=null;
    public final Integer year=null;
    public final Boolean working=null;
    public final String thumbnail=null;
}
