package fr.uavignon.ceri.MuseduCeri.data.webservice;

import android.content.ClipData;

import fr.uavignon.ceri.MuseduCeri.data.Item;

public class ItemResult {
    public final boolean isLoading;
    public final ItemResponse item;
    public final Throwable error;

    public ItemResult(boolean isLoading, ItemResponse item, Throwable error) {
        this.isLoading = isLoading;
        this.item = item;
        this.error = error;
    }

    public static void transferInfo(ItemResponse itemInfo, Item item, String key){
        item.setKey(key);
        item.setName(itemInfo.name);
        item.setDescription(itemInfo.description);
        item.setBrand(itemInfo.brand);
        item.setWorking(itemInfo.working);
        item.setTimeFrame(itemInfo.timeFrame);
        item.setYear(itemInfo.year);
        item.setThumbnail(itemInfo.thumbnail);

    }

}
