package fr.uavignon.ceri.MuseduCeri.data.webservice;

import java.util.Map;

public class CollectionResult {

    public final boolean isLoading;
    public final Map<String, ItemResponse> items;
    public final Throwable error;

  public  CollectionResult(boolean isLoading, Map<String, ItemResponse> items, Throwable error) {
        this.isLoading = isLoading;
        this.items = items;
        this.error = error;
    }
}
