package fr.uavignon.ceri.MuseduCeri.data.webservice;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface MusInterface {
    @Headers("Accept: application/geo+json")
    @GET("collection")
    Call<Map<String, ItemResponse>> getCollection();


}
