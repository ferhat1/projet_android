package fr.uavignon.ceri.MuseduCeri.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Entity(tableName = "item_table", indices = {@Index(value = {"name", "brand"},
        unique = true)})

public class Item {

    public static final long ADD_ID = -1;

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name="_id")
    private String key;


    @ColumnInfo(name="description")
    private  String description;

    @ColumnInfo(name="TimeFrame")
    private String TimeFrame;

    @ColumnInfo(name="name")
    private String name;


    @ColumnInfo(name="brand")
    private String brand;

    @ColumnInfo(name="year")
    private Integer year;

    @ColumnInfo(name="working")
    private Boolean working;

    @ColumnInfo(name="thumbnail")
    private String thumbnail;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getBrand() {
        return brand;
    }

    public void setBrand(@NonNull String brand) {
        this.brand = brand;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @NonNull
    public Boolean getWorking() {
        return working;
    }

    public void setWorking(@NonNull Boolean working) {
        this.working = working;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbNailBm) {
        this.thumbnail = thumbNailBm;
    }

    public void setTimeFrame(List<Integer> liste){
        String S = "";
        for(Integer L : liste){
            S = S + L.toString() + " ";
        }
        TimeFrame = S;
    }

    public String getTimeFrame(){
        return TimeFrame;
    }

    public void setTimeFrame(String M){
        TimeFrame = M;
    }

}
