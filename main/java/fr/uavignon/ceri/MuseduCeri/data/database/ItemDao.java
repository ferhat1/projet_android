package fr.uavignon.ceri.MuseduCeri.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.MuseduCeri.data.Item;

@Dao
public interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Item item);

    @Query("SELECT * from item_table ORDER BY name ASC")
    LiveData<List<Item>> getAllItems();


    @Query("DELETE FROM item_table WHERE _id = :id")
    void deleteItem(String id);

    @Query("SELECT * FROM item_table WHERE _id = :id")
    Item getItemById(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Item Item);
}
