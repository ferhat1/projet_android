package fr.uavignon.ceri.MuseduCeri.data.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.MuseduCeri.data.Item;

@Database(entities = {Item.class}, version = 1, exportSchema = false)
public abstract class MuseumRoomDatabase extends RoomDatabase {

    public abstract ItemDao itemDao();


    private static MuseumRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static MuseumRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MuseumRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    MuseumRoomDatabase.class,"museum_database")
                                    .build();
                }
            }
        }
        return INSTANCE;
    }


}
