package fr.uavignon.ceri.MuseduCeri.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.MuseduCeri.data.database.ItemDao;
import fr.uavignon.ceri.MuseduCeri.data.database.MuseumRoomDatabase;
import fr.uavignon.ceri.MuseduCeri.data.webservice.CollectionResult;
import fr.uavignon.ceri.MuseduCeri.data.webservice.ItemResponse;
import fr.uavignon.ceri.MuseduCeri.data.webservice.ItemResult;
import fr.uavignon.ceri.MuseduCeri.data.webservice.MusInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.MuseduCeri.data.database.MuseumRoomDatabase.databaseWriteExecutor;

public class ItemRepository {

    public static final String TAG = ItemRepository.class.getSimpleName();
    private static volatile ItemRepository INSTANCE;
    private final MusInterface api;



    private /*Mutable*/LiveData<List<Item>> allItems;
    private MutableLiveData<Item> selectedItem;
    private MutableLiveData<ItemResult> result;
    private ItemDao itemDao;

    public synchronized static ItemRepository get() {
        if (INSTANCE == null) {
            INSTANCE = new ItemRepository();
        }
        return INSTANCE;
    }

    public synchronized static ItemRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new ItemRepository(application);
        }

        return INSTANCE;
    }



    public ItemRepository(Application application) {
        this();
        Log.d(TAG, "public MuseumRepository(application) called");
        MuseumRoomDatabase db = MuseumRoomDatabase.getDatabase(application);
        itemDao = db.itemDao();
        allItems = itemDao.getAllItems();
        selectedItem = new MutableLiveData<>();
        result = new MutableLiveData<>();
    }


    public LiveData<List<Item>> getAllItems() {
        return allItems;
    }

    public MutableLiveData<Item> getSelectedItem() {
        return selectedItem;
    }

    public MutableLiveData<ItemResult> getResult() {
        return result;
    }



    public void getItem(String id)  {
        Future<Item> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return itemDao.getItemById(id);
        });
        try {
            selectedItem.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    private ItemRepository() {
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(MusInterface.class);
    }

    public MutableLiveData<CollectionResult> loadCollection() {
        final MutableLiveData<CollectionResult> result = new MutableLiveData<>();

        result.setValue(new CollectionResult(true, null, null));
        api.getCollection().enqueue(
                new Callback<Map<String, ItemResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, ItemResponse>> call,
                                           Response<Map<String, ItemResponse>> response) {
                        Log.d(TAG,"onResponse for loadCollection");

                        if(response.body() != null){
                            Log.d(TAG,"Items :");
                            for(Map.Entry<String, ItemResponse> e : response.body().entrySet()){
                                Log.d(TAG,e.getKey()+" - " + e.getValue().description);
                            }
                        }else Log.d(TAG,"Null result for loadCollection");
                        result.postValue(new CollectionResult(false, response.body(), null));

                        for(Map.Entry<String, ItemResponse> e : response.body().entrySet()){
                            Item item = new Item();
                            ItemResult.transferInfo(e.getValue(), item,e.getKey());

                            new Thread(new Runnable(){
                                @Override
                                public void run() {
                                    itemDao.insert(item);
                                }
                            }).start();

                        }
                    }
                    @Override
                    public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {
                        Log.d(TAG,"onFailure for loadCollection "+t.getMessage());
                        result.postValue(new CollectionResult(false, null, t));
                    }
                });

        return result;
    }

}
