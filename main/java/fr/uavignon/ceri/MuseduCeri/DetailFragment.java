package fr.uavignon.ceri.MuseduCeri;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

public class DetailFragment extends Fragment {
    public static final String TAG = fr.uavignon.ceri.MuseduCeri.DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textTitre, textDescription, textTBrand, textYear, textTimeFrame, textWorking;
    private ImageView imageView;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected city
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String itemID = args.getItemId();
        Log.d(TAG,"selected id="+itemID);
        viewModel.setItem(itemID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textTitre = getView().findViewById(R.id.Titre);
        textDescription = getView().findViewById(R.id.Description);
        textYear = getView().findViewById(R.id.Year);
        textWorking = getView().findViewById(R.id.Working);
        textTimeFrame = getView().findViewById(R.id.TimeFrame);
        textTBrand = getView().findViewById(R.id.Brand);
        textTimeFrame = getView().findViewById(R.id.TimeFrame);
        imageView =getView().findViewById(R.id.imageView2);






        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.MuseduCeri.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {

        viewModel.getItem().observe(getViewLifecycleOwner(),
                item -> {
                    if (item != null) {
                        Log.d(TAG, "observing city view");
                        Log.d(TAG, "observing city view");
                        textTitre.setText(item.getName());
                        textDescription.setText(item.getDescription());
                        //if (item.getYear() != null)
                        if(textTBrand.getText() != null){
                            textTBrand.setText(item.getBrand());
                        } else{
                            textTBrand.setText("Vide");
                        }

                        if(item.getYear() != null){
                            textYear.setText(item.getYear().toString());
                        }else{
                            textYear.setText("Vide");
                        }
                        if(item.getWorking() != null){
                            textWorking.setText(item.getWorking().toString());
                        }else{
                            textWorking.setText("Vide");
                        }
                        if(item.getTimeFrame() != null){
                            textTimeFrame.setText(item.getTimeFrame());
                        }else{
                            textTimeFrame.setText("Vide");
                        }

                    }
                    Glide.with(getContext())
                            .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+ item.getKey() +"/thumbnail"/**/)
                            .into(imageView);

                });



    }


}