package fr.uavignon.ceri.MuseduCeri;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import fr.uavignon.ceri.MuseduCeri.data.Item;

public class ListFragment extends Fragment {

    public static final String TAG = ListFragment.class.getSimpleName();


    private ListViewModel viewModel;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapter adapter;
    private ProgressBar progress;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ListViewModel.class);
        listenerSetup();
        observerSetup();
    }

    private void listenerSetup() {
        recyclerView = getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter(getContext());
        recyclerView.setAdapter(adapter);
        adapter.setListViewModel(viewModel);
        progress = getView().findViewById(R.id.progressList);

    }

    private void observerSetup() {
        viewModel.getAllItems().observe(getViewLifecycleOwner(),
                items -> {
                    adapter.setItemList(items);
                    progress.setVisibility(View.GONE);
                });

        viewModel.loadCollection();
        ((MainActivity)(getParentFragment().getActivity())).requestLoadAllItems.observe(getViewLifecycleOwner(),
                request -> {
                    if(request) {
                        viewModel.loadCollection();
                        progress.setVisibility(View.VISIBLE);
                        ((MainActivity)(getParentFragment().getActivity())).requestLoadAllItems.setValue(Boolean.FALSE);
                    }
                });

    }
}