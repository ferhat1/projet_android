package fr.uavignon.ceri.MuseduCeri;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.Map;

import fr.uavignon.ceri.MuseduCeri.data.Item;
import fr.uavignon.ceri.MuseduCeri.data.ItemRepository;
import fr.uavignon.ceri.MuseduCeri.data.webservice.CollectionResult;
import fr.uavignon.ceri.MuseduCeri.data.webservice.ItemResponse;

public class ListViewModel extends AndroidViewModel {
    public static final String TAG = ListViewModel.class.getSimpleName();

    private ItemRepository repository;
    private LiveData<List<Item>> allItems;
    /*Mutable*/LiveData<CollectionResult> lastResult;
    LiveData<Map<String, ItemResponse>> results;

    public ListViewModel (Application application) {
        super(application);
        repository = ItemRepository.get(application);
        allItems = repository.getAllItems();
    }

    LiveData<List<Item>> getAllItems() {
        return allItems;
    }


    void loadCollection() {

        results = (LiveData<Map<String, ItemResponse>>) repository.loadCollection().getValue().items;

    }


}
